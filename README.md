This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# City Weather frontend

City Weather backed: [https://bitbucket.org/andr172/city_weather_backend/src/master/](https://bitbucket.org/andr172/city_weather_backend/src/master/)

#### This app will show you basic weather information about your city in real-time!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm run build
```