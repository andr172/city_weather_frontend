import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import UpdateCityForm from '../components/forms/UpdateCityForm';
import UpdatePassword from '../components/forms/UpdatePassword';
import RemoveAccount from '../components/RemoveAccount';

function Account() {
  return (
    <div>
      <h2>Update profile</h2>
      <br />
      <Tabs defaultActiveKey="city" id="update-profile-tabs">
        <Tab eventKey="city" title="City">
          <UpdateCityForm />
        </Tab>
        <Tab eventKey="password" title="Password">
          <UpdatePassword />
        </Tab>
        <Tab eventKey="remove" title="Remove">
          <div className="text-center">
            <h5 className="mt-5 mb-5">This action will completely remove your account!</h5>
            <RemoveAccount />
          </div>
        </Tab>
      </Tabs>
    </div>
  );
}

export default Account;
