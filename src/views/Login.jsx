import React from 'react';
import LoginForm from '../components/forms/LoginForm';

function Login() {
  return (
    <div>
      <h2 className="text-center">Login</h2>
      <br />
      <LoginForm />
    </div>
  );
}

export default Login;
