import React from 'react';
import UserListGroup from '../components/admin/UserListGroup';

function UserList() {
  return (
    <div>
      <UserListGroup />
    </div>
  );
}

export default UserList;
