import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getMyCity } from '../store/actions/cityActions';
import WeatherInfo from '../components/ui/WeatherInfo';

class Home extends React.Component {
  async componentDidMount() {
    await this.props.getMyCity();
  }

  render() {
    return (
      <WeatherInfo />
    );
  }
}

Home.propTypes = {
  getMyCity: PropTypes.func.isRequired,
};

export default connect(null, { getMyCity })(Home);
