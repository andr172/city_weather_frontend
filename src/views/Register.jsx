import React from 'react';
import RegisterForm from '../components/forms/RegisterForm';

function Register() {
  return (
    <div>
      <h2 className="text-center">Register</h2>
      <br />
      <RegisterForm submitTitle="Submit" loginUser />
    </div>
  );
}

export default Register;
