export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const DISPLAY_NOTIFICATION = 'DISPLAY_NOTIFICATION';
export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION';
export const GET_MY_CITY = 'GET_MY_CITY';
export const REMOVE_MY_CITY = 'REMOVE_MY_CITY';
export const SET_LOADING = 'SET_LOADING';
