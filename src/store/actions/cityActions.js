/* eslint-disable no-underscore-dangle */
import ApiService from '../../services/api.service';
import {
  GET_MY_CITY,
  REMOVE_MY_CITY,
  SET_LOADING,
} from './types';
import io from '../../services/socket.service';

/**
 * Get city information
 */
export const getMyCity = () => async (dispatch) => {
  try {
    // Set loading
    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    const res = await ApiService.get('cities/my');
    if (res.status === 200) {
      dispatch({
        type: GET_MY_CITY,
        payload: res.data.city,
      });
    }

    // Listen for updates with socket
    io.listenCity(String(res.data.city._id));
  } catch (err) {
    // Unset loading
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
  }
};

/**
 * Remove city information from store
 */
export const removeMyCity = () => (dispatch) => {
  dispatch({
    type: REMOVE_MY_CITY,
  });
};
