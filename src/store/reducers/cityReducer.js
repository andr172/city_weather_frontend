import {
  GET_MY_CITY,
  REMOVE_MY_CITY,
  SET_LOADING,
} from '../actions/types';

const initialState = {
  loading: false,
  apiId: null,
  description: '',
  humidity: null,
  name: '',
  pressure: null,
  temp: null,
  weatherCode: null,
  tempGrows: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_MY_CITY:
      return {
        apiId: action.payload.apiId,
        description: action.payload.description,
        humidity: action.payload.humidity,
        name: action.payload.name,
        pressure: action.payload.pressure,
        temp: action.payload.temp,
        weatherCode: action.payload.weatherCode,
        tempGrows: action.payload.tempGrows,
        loading: false,
      };
    case REMOVE_MY_CITY:
      return {
        ...initialState,
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
}
