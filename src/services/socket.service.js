import io from 'socket.io-client';
import store from '../store/store';
import { GET_MY_CITY } from '../store/actions/types';

let socket = null;

export default {
  /**
   * Connect socket
   */
  connect() {
    socket = io(process.env.REACT_APP_ROOT);
  },

  /**
   * Join room to listen for city information change
   * @param {string} cityId
   */
  listenCity(cityId) {
    socket.emit('room', cityId);

    // When city-info event is emitted
    socket.on('city-info', (data) => {
      // Save city information to store
      store.dispatch({
        type: GET_MY_CITY,
        payload: data,
      });
    });
  },

  /**
   * Disconnect socket
   */
  disconnect() {
    socket.disconnect();
    socket = null;
  },
};
