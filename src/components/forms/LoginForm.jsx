/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { login } from '../../store/actions/userActions';

class LoginForm extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-sm-11 col-md-6 col-lg-4">
            <Formik
              initialValues={{ emailOrUsername: '', password: '' }}
              validationSchema={Yup.object({
                emailOrUsername: Yup.string()
                  .required('*Username or Email is required'),
                password: Yup.string()
                  .min(6, 'Password must have at least 6 characters')
                  .required('Password is required'),
              })}
              onSubmit={async (form) => {
                await this.props.login(form);
              }}
            >
              {({ touched, errors, isSubmitting }) => (
                <Form>
                  <div className="form-group">
                    <label htmlFor="email">Email or Username</label>
                    <Field
                      type="text"
                      name="emailOrUsername"
                      placeholder="Enter email or username"
                      className={`form-control ${
                        touched.emailOrUsername && errors.emailOrUsername ? 'is-invalid' : ''
                      }`}
                    />
                    <ErrorMessage
                      component="div"
                      name="emailOrUsername"
                      className="invalid-feedback"
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <Field
                      type="password"
                      name="password"
                      placeholder="Enter password"
                      className={`form-control ${
                        touched.password && errors.password ? 'is-invalid' : ''
                      }`}
                    />
                    <ErrorMessage
                      component="div"
                      name="password"
                      className="invalid-feedback"
                    />
                  </div>


                  <div className="text-center mt-3">
                    <Button
                      type="submit"
                      variant="info"
                      className="mr-5"
                      disabled={isSubmitting}
                    >
                      {isSubmitting ? 'Please wait...' : 'Submit'}
                    </Button>
                    <span>
                      <NavLink to="/register">Register</NavLink>
                    </span>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

export default connect(null, { login })(LoginForm);
