/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import ApiService from '../../services/api.service';
import { showNotification } from '../../store/actions/notificationActions';

class UpdateCityForm extends React.Component {
  render() {
    let showCityId = false;
    return (
      <div className="row mt-3">
        <div className="col-12 col-lg-8">
          <Formik
            initialValues={{
              city: '',
              cityId: '',
            }}
            validationSchema={Yup.object({
              city: Yup.string().required('City is required'),
            })}
            onSubmit={async (form) => {
              try {
                if (!form.cityId) {
                  await ApiService.post('/cities/test-name', { name: form.city });
                  showCityId = false;
                }
                const res = await ApiService.put('/users/change_city', { form });
                if (res.status === 200) {
                  this.props.showNotification({
                    text: 'City changed successfully',
                    title: 'Update',
                    type: 'success',
                  });
                }
              } catch (err) {
                if (err.response.status === 404) {
                  showCityId = true;
                }
              }
            }}
          >
            {({ touched, errors, isSubmitting }) => (
              <Form>
                <div className="row mb-5">
                  <div className="col-12 col-md-6">
                    <label htmlFor="city">New City</label>
                    <Field
                      type="text"
                      name="city"
                      placeholder="Enter your city name"
                      className={`form-control ${
                        touched.city && errors.city ? 'is-invalid' : ''
                      }`}
                    />
                    <ErrorMessage
                      component="div"
                      name="city"
                      className="invalid-feedback"
                    />
                  </div>
                  {
                    showCityId ? (
                      <div className="col-12 col-md-6">
                        <label htmlFor="cityId">City ID</label>
                        <Field
                          type="number"
                          name="cityId"
                          min="0"
                          step="1"
                          placeholder="Enter your city ID"
                          className={`form-control ${
                            touched.cityId && errors.cityId ? 'is-invalid' : ''
                          }`}
                        />
                        <ErrorMessage
                          component="div"
                          name="cityId"
                          className="invalid-feedback"
                        />
                        <small>We coludn&apos;t find your city, please enter city id.</small>
                        <br />
                        <small>You can find city id after searching your city in link below.</small>
                        <br />
                        <small>It is the number in browser url.</small>
                        <br />
                        <a
                          href="https://openweathermap.org/city"
                          target="_blank"
                          rel="noopener noreferrer"
                        >

                          Open weather forecast
                        </a>
                      </div>
                    ) : null
                  }
                </div>

                <div className="mt-3">
                  <Button
                    type="submit"
                    variant="info"
                    disabled={isSubmitting}
                  >
                    {isSubmitting ? 'Please wait...' : 'Change'}
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

UpdateCityForm.propTypes = {
  showNotification: PropTypes.func.isRequired,
};

export default connect(null, { showNotification })(UpdateCityForm);
