/* eslint-disable class-methods-use-this */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Image from 'react-bootstrap/Image';

// images
import { FaArrowUp, FaArrowDown } from 'react-icons/fa';
import clear from '../../assets/weather/clear.png';
import clouds from '../../assets/weather/clouds.png';
import drizzle from '../../assets/weather/drizzle.png';
import rain from '../../assets/weather/rain.png';
import snow from '../../assets/weather/snow.png';
import tunderstorm from '../../assets/weather/tunderstorm.png';
import mist from '../../assets/weather/mist.png';

class WeatherInfo extends React.Component {
  constructor(props) {
    super(props);
    this.getImageSrc = this.getImageSrc.bind(this);
    this.getIcon = this.getIcon.bind(this);
  }

  /**
   * Get weather image based on weathercode
   * @param {number} code
   * @returns {*}
   */
  getImageSrc(code) {
    if (!code) {
      return null;
    }

    // get first and last number in code
    const firstNum = code.toString().charAt(0);
    const lastNum = code.toString().charAt(2);

    switch (firstNum) {
      case '2': return tunderstorm;
      case '3': return drizzle;
      case '5': return rain;
      case '6': return snow;
      case '7': return mist;
      case '8': {
        if (lastNum === '0') return clear;
        return clouds;
      }
      default: return null;
    }
  }

  /**
   * Get arrow icon based on temperature change
   * @param {string} info
   * @returns {*}
   */
  getIcon(info) {
    switch (info) {
      case 'up': return <FaArrowUp color="green" />;
      case 'down': return <FaArrowDown color="red" />;
      default: return null;
    }
  }

  render() {
    const weather = this.getImageSrc(this.props.city.weatherCode);
    const icon = this.getIcon(this.props.city.tempGrows);
    return !this.props.city.loading ? (
      <div className="text-center">
        <h1>
          Weather in
          {' '}
          {this.props.city.name}
        </h1>
        <div className="d-flex justify-content-center">
          <div>
            <Image src={weather} width="100" height="100" fluid />
            <p className="font-weight-bold">{this.props.city.description}</p>
            <p>
              humidity:
              {' '}
              {this.props.city.humidity}
              {' '}
              %
            </p>
            <p>
              pressure:
              {' '}
              {this.props.city.pressure}
              {' '}
              hpa
            </p>
          </div>
          <h1 className="mt-5 ml-5 text-uppercase font-weight-bold">
            {this.props.city.temp}
            {' '}
            °C
            {' '}
            {icon}
          </h1>
        </div>

      </div>
    ) : (
      <div className="p-5 text-center font-weight-bold">
        Loading...
      </div>
    );
  }
}

WeatherInfo.propTypes = {
  city: PropTypes.shape({
    weatherCode: PropTypes.number,
    temp: PropTypes.number,
    pressure: PropTypes.number,
    humidity: PropTypes.number,
    name: PropTypes.string,
    tempGrows: PropTypes.string,
    description: PropTypes.string,
    loading: PropTypes.bool,
  }).isRequired,
};

const mapStateToProps = (state) => ({
  city: state.city,
});
export default connect(mapStateToProps, null)(WeatherInfo);
