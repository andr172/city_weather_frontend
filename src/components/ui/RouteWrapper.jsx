import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';

function RouteWrapper(props) {
  return (
    <Container style={{ paddingTop: '5rem', paddingBottom: '5rem', minHeight: '100vh' }}>
      {props.children}
    </Container>
  );
}

RouteWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

export default RouteWrapper;
