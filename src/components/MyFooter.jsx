import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function MyFooter() {
  return (
    <div className="pb-5 pt-3 my-footer">
      <Row className="justify-content-center m-auto">
        <Col className="col-10 col-md-6 col-lg-4 mt-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </Col>
      </Row>
      <Row className="m-0 justify-content-center">
        <Col className="text-left mt-4 ml-5 col-10 col-sm-5">
          &#9400; 2019 All rights reserved
        </Col>
        <Col className="m-0 text-right mt-4 col-12 col-sm-8 col-md-5">
          <span>Privacy policy</span>
          <span className="mr-4 ml-4">Link</span>
          <span>Lorem ipsum</span>
        </Col>
      </Row>
    </div>
  );
}

export default MyFooter;
